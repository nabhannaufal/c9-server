"use strict";
const { Model } = require("sequelize");

const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    static associate(models) {}
    static #encrypt = (password) => bcrypt.hashSync(password, 10);
    static register = ({
      username,
      password,
      email,
      total_score,
      bio,
      city,
      social_media_url,
    }) => {
      const encryptedPassword = this.#encrypt(password);
      const defaultScore = 0;
      const defaultSocialUrl = `http://social.${username}.com`;
      const defaultBio = `Ini adalah biodata dari ${username}`;

      return this.create({
        username,
        password: encryptedPassword,
        email,
        total_score: defaultScore,
        bio: defaultBio,
        city,
        social_media_url: defaultSocialUrl,
      });
    };

    checkPassword = (password) => bcrypt.compareSync(password, this.password);
    generateToken = () => {
      const payload = {
        id: this.id,
        username: this.username,
      };
      const rahasia = "Ini rahasia ga boleh disebar-sebar";
      const token = jwt.sign(payload, rahasia);
      return token;
    };
    static authenticate = async ({ username, password }) => {
      try {
        const user = await this.findOne({ where: { username } });
        if (!user) return Promise.reject("User not found!");
        const isPasswordValid = user.checkPassword(password);
        if (!isPasswordValid) return Promise.reject("Wrong password");
        return Promise.resolve(user);
      } catch (err) {
        return Promise.reject(err);
      }
    };
  }
  User.init(
    {
      username: DataTypes.STRING,
      password: DataTypes.STRING,
      email: DataTypes.STRING,
      total_score: DataTypes.INTEGER,
      bio: DataTypes.STRING,
      city: DataTypes.STRING,
      social_media_url: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "User",
    }
  );
  return User;
};
